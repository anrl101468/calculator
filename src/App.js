import './App.css';
import {Main} from "./state/page/Main";

function App() {
  return (
    <div className="App">
      <Main/>
    </div>
  );
}

export default App;
