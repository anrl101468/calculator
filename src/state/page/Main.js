import Box from '@mui/material/Box';
import {CustomInput} from "../components/CustomInput";
import {useStores} from "../context/Context";
import {ButtonGrid} from "../components/ButtonGrid";
import {useState} from "react";
import {observer} from "mobx-react-lite";
import {TextField} from "@mui/material";

export const Main = observer(() =>
{
    const {calculatorStore} = useStores();
    const [operator, setOperator] = useState(null);
    const [num1, setNum1] = useState('');
    const [num2, setNum2] = useState('');
    const [result,setResult] = useState('');

    const OPERATORS = ["+", "-", "*", "/"];
    const isNumber = value => !isNaN(value);
    const isOperator = value => OPERATORS.includes(value);

    const OperatorAction = {
        "+": (firstVal, secondVal) => firstVal + secondVal,
        "-": (firstVal, secondVal) => firstVal - secondVal,
        "*": (firstVal, secondVal) => firstVal * secondVal,
        "/": (firstVal, secondVal) => firstVal / secondVal
    };

    const lines = [
        calculatorStore.oneLine,
        calculatorStore.twoLine,
        calculatorStore.treeLine,
        calculatorStore.fourLine,
        calculatorStore.fiveLine
    ];

    const handleClick = (value) => {
        let newNum1 = num1;
        let newNum2 = num2;
        let newOperator = operator;

        // check if the value is a number
        if(!isNaN(value) || value === '.') {
            if(operator === null) {
                newNum1 += value.trim();
                setNum1(newNum1);
            } else {
                newNum2 += value.trim();
                setNum2(newNum2);
            }
            // now check if the value is an operator
        } else if(["+", "-", "*", "/"].includes(value)) {
            newOperator = value;
            setOperator(newOperator);
        } else {
            switch (value)
            {
                case 'X' :
                    if(operator) {
                        newNum2 = newNum2.trim().substring(0,newNum2.length-1);
                        setNum2(newNum2);
                    } else {
                        newNum1 = newNum1.trim().substring(0,newNum1.length-1);
                        setNum1(newNum1);
                    }
                    break;
                case 'C' :
                    newNum1 = '';
                    newNum2 = '';
                    newOperator = null;
                    setNum1(newNum1);
                    setNum2(newNum2);
                    setOperator(newOperator);
                    calculatorStore.resetResltHistory();
                    break;
                case '=' :
                    if (newOperator && newNum2 !== '') {
                        newNum1 = OperatorAction[newOperator](Number(newNum1), Number(newNum2)).toString();
                        setNum1(newNum1);
                        newNum2 = '';
                        setNum2(newNum2);
                        newOperator = null;
                        setOperator(newOperator);
                        calculatorStore.setResultHistory(newNum1);
                    }
                    break;
                case '<-' :
                    if(calculatorStore.resultHistory.length-1 > 0)
                    {
                        newNum1 = calculatorStore.resultHistory[calculatorStore.resultHistory.length-2];
                        calculatorStore.removeResultHistory();
                        setNum1(newNum1);
                        newNum2 = '';
                        setNum2(newNum2);
                        newOperator = null;
                        setOperator(newOperator);
                    }
                    else
                    {
                        alert("더이상 되돌릴수 없습니다");
                    }
                    break;
            }
        }
        setResult(`${newNum1}${newOperator ?? ''}${newNum2}`);
    }

    return (
        <Box sx={{display : 'flex'}}>
            <Box component="section"
                 sx={{
                     p: 2,
                     border: '1px dashed grey',
                     width: '300px',
                     height: '350px'
                 }}
            >
                <Box
                    sx={{
                        width: '100%',
                        height: '50px',
                    }}
                >
                    <CustomInput result={result}/>
                </Box>
                {lines.map((line, i) => (
                    <ButtonGrid buttons={line} index={i} key={i} click={ (e) => handleClick(e.target.value)}/>
                ))}
            </Box>
            <Box
                sx={{
                    p: 2,
                    border: '1px dashed grey',
                    width: '100px',
                    height: '350px',
                    overflowY: 'auto'
                }}
            >
                {calculatorStore.resultHistory.map((r,i) =>
                <TextField value={r}
                    InputProps={{readOnly: true}}/>
                )}
            </Box>
        </Box>
    );
})
