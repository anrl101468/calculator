import {observable} from "mobx";

export const CalculatorStore = () =>
{
    const store = observable({
        oneLine : ['+','-','*','/'],
        twoLine : ['7','8','9','X'],
        treeLine : ['4','5','6','C'],
        fourLine : ['1','2','3','<-'],
        fiveLine : [' ','0','.','='],
        resultHistory : [],
        setResultHistory (value) {
            store.resultHistory.push(value);
        },

        removeResultHistory () {
            store.resultHistory.pop();
        },
        resetResltHistory () {
            store.resultHistory = []
        }
    });

    return store;
}
