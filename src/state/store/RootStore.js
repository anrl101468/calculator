import { observable } from "mobx";
import {CalculatorStore} from "./calculatorStore/CalculatorStore";

export const RootStore = () => {
    const store = observable({
        calculatorStore : CalculatorStore(),
    });

    return store;
};
