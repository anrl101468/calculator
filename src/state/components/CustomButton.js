import Button from '@mui/material/Button';
export const CustomButton = (p) =>
{
    return (
        <Button variant="contained"
            sx={{margin : '1px'}}
            color={p.color}
            disabled={p.disabled}
            onClick={p.click}
            value={p.text}
        >
        {p.text}
        </Button>
    )
}
