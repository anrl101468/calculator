import {TextField} from "@mui/material";
export const CustomInput = (p) =>
{
    return (
        <TextField
            fullWidth
            id="standard-read-only-input"
            label="Result"
            InputProps={{
                readOnly: true,
            }}
            variant="filled"
            sx={{ height: '50px' }}
            value={p.result}
        />
    )
}
