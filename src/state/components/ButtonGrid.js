import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import {CustomButton} from "./CustomButton";

export const ButtonGrid = ({ buttons , index, click}) => (
    <Box
        key={index}
        sx={{
            width: '300px',
            height: '50px',
            marginTop : index == 0 ? '20px' : '5px'
        }}
    >
        <Grid
            container
            spacing={0}
            sx={{
                width: '100%',
                height: '100%',
                padding: '0 15px'
            }}
        >
            {buttons.map((x,i) => {
                return (
                    <CustomButton text={x}
                        key={i}
                        click ={click}
                        style={{ width : '100%', height : '100%'}}
                        disabled={x === ' '}
                    />
                )
            })}
        </Grid>
    </Box>
);
